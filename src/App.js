import React from "react";
import { Routes, Route } from "react-router-dom";
import { useState, useEffect } from "react";
import Wrapper from "./components/Wrapper/Wrapper";
import Header from "./pages/Header/Header";
import CartPage from "./pages/CartPage/CartPage";
import FavPage from "./pages/FavPage/FavPage";
import Main from "./pages/Main/Main";
import NoPage from "./pages/NoPage/NoPage";

function App() {
  const [tryToCart, setTryToCart] = useState({});
  const [favorite, setFavorite] = useState([]);
  const [modal, setModal] = useState(false);
  const [cart, setCart] = useState([]);

  useEffect(() => {
    getCart();
    getFavorite();
  }, []);

  const openModal = () => {
    setModal(true);
  };
  const closeModal = () => {
    setModal(false);
  };

  const getCart = () => {
    let cart = JSON.parse(localStorage.getItem("cart"));
    if (cart && cart.length > 0) {
      setCart(cart);
    }
  };

  const getFavorite = () => {
    let favorite = JSON.parse(localStorage.getItem("favorite"));
    if (favorite) {
      setFavorite(favorite);
    }
  };

  const addProdToCart = () => {
    let oldCart = [...cart];
    let item = { ...tryToCart };
    oldCart.push(item);
    setCart(oldCart);
    localStorage.setItem("cart", JSON.stringify(oldCart));
  };
  const removeProdFromCart = () => {
    let oldCart = [...cart];
    let item = { ...tryToCart };
    const newCart = oldCart.filter((product) => product.id !== item.id);
    setCart(newCart);
    localStorage.setItem("cart", JSON.stringify(newCart));
  };

  const addProdToFavourite = (thisCard) => {
    let oldFavorites = [...favorite];
    let newFavorites = oldFavorites.concat(thisCard);
    setFavorite(newFavorites);
    localStorage.setItem("favorite", JSON.stringify(newFavorites));
  };

  const removeProdFromFavourite = (id) => {
    const oldFavorites = [...favorite];
    const newFavorites = oldFavorites.filter((product) => product.id !== id);
    setFavorite(newFavorites);
    localStorage.setItem("favorite", JSON.stringify(newFavorites));
  };

  return (
    <>
      <Wrapper>
        <Header cartAmount={cart.length} favoriteAmount={favorite.length} />

        <Routes>
          <Route
            path="/"
            element={
              <Main
                setTryToCart={setTryToCart}
                addProdToCart={addProdToCart}
                addProdToFavourite={addProdToFavourite}
                removeProdFromFavourite={removeProdFromFavourite}
                favorite={favorite}
                openModal={openModal}
                closeModal={closeModal}
                modal={modal}
              />
            }
          />

          <Route
            path="/cart"
            element={
              <CartPage
                addProdToCart={addProdToCart}
                addProdToFavourite={addProdToFavourite}
                removeProdFromFavourite={removeProdFromFavourite}
                cart={cart}
                favorite={favorite}
                openModal={openModal}
                closeModal={closeModal}
                modal={modal}
                setTryToCart={setTryToCart}
                removeProdFromCart={removeProdFromCart}
              />
            }
          />
          <Route
            path="/favourites"
            element={
              <FavPage
                addProdToCart={addProdToCart}
                addProdToFavourite={addProdToFavourite}
                removeProdFromFavourite={removeProdFromFavourite}
                favorite={favorite}
                openModal={openModal}
                closeModal={closeModal}
                modal={modal}
                setTryToCart={setTryToCart}
              />
            }
          />
          <Route path="*" element={<NoPage />} />
        </Routes>
      </Wrapper>
    </>
  );
}

export default App;
