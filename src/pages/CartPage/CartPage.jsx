import "./CartPage.scss";
import Modal from "../../components/Modals/Modal";
import CartItem from "../../components/Cart/Cart";
import EmptyCartIcon from "./IconEmptyCart/IconEmptyCart";
import PropTypes from "prop-types";

function CartPage(props) {
  const {
    removeProdFromCart,
    cart,
    openModal,
    closeModal,
    modal,
    setTryToCart,
  } = props;

  const btnMessage = "Delete from cart";
  let key = 1;
  let totalPrice = cart.reduce((acc, item) => acc + Number(item.price), 0);
  console.log(totalPrice);
  return (
    <>
      {Number(cart.length) > 0 ? (
        <>
          <Modal
            active={modal}
            header="Delete this item from cart?"
            textContent="Are you sure you want to delete this item to the card? "
            closeModalWindow={closeModal}
            submitBtnFunc={removeProdFromCart}
          />

          <div className="cart-section">
            <ul className="cart__list">
              {cart.map((cartItem) => (
                <CartItem
                  id={cartItem.id}
                  key={key++}
                  name={cartItem.name}
                  price={cartItem.price}
                  image={cartItem.image}
                  vin={cartItem.vin}
                  toOpenModal={openModal}
                  tryToAddToCart={setTryToCart}
                  btnMessage={btnMessage}
                />
              ))}
            </ul>
            <div className="cart__total-price">
              Total price:{" "}
              {new Intl.NumberFormat("en-US", {
                style: "currency",
                currency: "UAH",
                currencyDisplay: "narrowSymbol",
              }).format(totalPrice)}
            </div>
          </div>
        </>
      ) : (
        <>
          <EmptyCartIcon />
        </>
      )}
    </>
  );
}
CartPage.propTypes = {
  removeProdFromCart: PropTypes.func.isRequired,
  cart: PropTypes.array.isRequired,
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  setTryToCart: PropTypes.func.isRequired,
  modal: PropTypes.bool.isRequired,
};
export default CartPage;
