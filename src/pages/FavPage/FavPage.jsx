import "./FavPage.scss";
import Modal from "../../components/Modals/Modal";
import Card from "../../components/Card/Card";
import EmptyFavoriteIcon from "./IconEmptyFav/IconEmptyFav";
import PropTypes from "prop-types";

function FavouritesPage(props) {
  const {
    setTryToCart,
    modal,
    closeModal,
    openModal,
    addProdToCart,
    addProdToFavourite,
    removeProdFromFavourite,
    favorite,
  } = props;
  const btnMessage = "Add to cart";
  return (
    <>
      {Number(favorite.length) > 0 ? (
        <>
          <Modal
            active={modal}
            header="Add this item to the cart?"
            textContent="Are you sure you want to add this item to the card? "
            closeModalWindow={closeModal}
            submitBtnFunc={addProdToCart}
          />

          <div className="favorite__section">
            <ul className="favorite__list">
              {favorite.map((fav) => (
                <Card
                  id={fav.id}
                  key={fav.id}
                  name={fav.name}
                  price={fav.price}
                  image={fav.image}
                  vin={fav.vin}
                  toOpenModal={openModal}
                  tryToAddToCart={setTryToCart}
                  addProdToFavourite={addProdToFavourite}
                  removeProdFromFavourite={removeProdFromFavourite}
                  favorite={favorite}
                  btnMessage={btnMessage}
                />
              ))}
            </ul>
          </div>
        </>
      ) : (
        <>
          <EmptyFavoriteIcon />
        </>
      )}
    </>
  );
}

FavouritesPage.propTypes = {
  addProdToCart: PropTypes.func.isRequired,
  addProdToFavourite: PropTypes.func.isRequired,
  removeProdFromFavourite: PropTypes.func.isRequired,
  favorite: PropTypes.array.isRequired,
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  setTryToCart: PropTypes.func.isRequired,
  modal: PropTypes.bool.isRequired,
};
export default FavouritesPage;
