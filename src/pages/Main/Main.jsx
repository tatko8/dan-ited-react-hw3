import "./Main.scss";
import axios from "axios";
import { useEffect, useState } from "react";
import Card from "../../components/Card/Card";
import Modal from "../../components/Modals/Modal";
import PropTypes from "prop-types";

function MainSection(props) {
  const {
    setTryToCart,
    addProdToCart,
    addProdToFavourite,
    removeProdFromFavourite,
    favorite,
    closeModal,
    openModal,
    modal,
  } = props;

  const [products, setProducts] = useState([]);
  const btnMessage = "Add to cart";
 
  useEffect(() => {
    const fetchData = async () => {
      const response = await axios
        .get(`${document.location.href}books.json`)
        .catch((err) => {
          console.warn(err);
          alert("Something went wrong. Try later.");
        });
      setProducts(response.data);
    };
    fetchData();
  }, []);

  return (
    <>
      <Modal
        active={modal}
        header="Add this item to the cart?"
        textContent="Are you sure you want to add this item to the card? "
        closeModalWindow={closeModal}
        submitBtnFunc={addProdToCart}
      />

      <div className="main-section">
        <ul className="main-section__list">
          {products.map((product) => (
            <Card
              id={product.id}
              key={product.id}
              name={product.name}
              price={product.price}
              image={product.image}
              vin={product.vin}
              toOpenModal={openModal}
              tryToAddToCart={setTryToCart}
              addProdToFavourite={addProdToFavourite}
              removeProdFromFavourite={removeProdFromFavourite}
              favorite={favorite}
              btnMessage={btnMessage}
            />
          ))}
        </ul>
      </div>
    </>
  );
}
MainSection.propTypes = {
  setTryToCart: PropTypes.func.isRequired,
  addProdToCart: PropTypes.func.isRequired,
  addProdToFavourite: PropTypes.func.isRequired,
  removeProdFromFavourite: PropTypes.func.isRequired,
  favorite: PropTypes.array.isRequired,
  closeModal: PropTypes.func.isRequired,
  openModal: PropTypes.func.isRequired,
  modal: PropTypes.bool.isRequired,
};
export default MainSection;

