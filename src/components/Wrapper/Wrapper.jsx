import "./Wrapper.scss";
import PropTypes from "prop-types";
function Wrapper({ children }) {
  return <div className="Wrapper">{children}</div>;
}

Wrapper.propTypes = {
  children: PropTypes.array,
};
export default Wrapper;
