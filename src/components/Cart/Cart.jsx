import PropTypes from "prop-types";
import "./Cart.scss";
import AddToCart from "../Buttons/Button";

function CartItem(props) {
  const {
    id,
    tryToAddToCart,
    btnMessage,
    name,
    image,
    vin,
    price,
    toOpenModal,
  } = props;

  const thisCard = { id, name, price, image, vin };

  const tryToAddProdToCart = () => {
    tryToAddToCart(thisCard);
  };
  return (
    <div className="cartItem">
      <div className="cartItem__img">
        <img
          className="cartItem__image"
          src={image}
          alt="card of product in the cart"
        ></img>
      </div>
      <div className="cartItem__info">
        <p className="cartItem__name">{name} </p>
        <p className="cartItem__vin"> Article: {vin}</p>
        <p className="cartItem__price">
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "UAH",
            currencyDisplay: "narrowSymbol",
          }).format(price)}
        </p>
      </div>
      <div className="cartItem__action">
        <AddToCart
          backgroundColor="rgb(116, 34, 50)"
          text={btnMessage}
          openModalfunc={toOpenModal}
          addToCartFunc={tryToAddProdToCart}
        />
      </div>
    </div>
  );
}
CartItem.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  vin: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired,
  btnMessage: PropTypes.string.isRequired,
  tryToAddToCart: PropTypes.func.isRequired,
  toOpenModal: PropTypes.func.isRequired,
};
export default CartItem;
