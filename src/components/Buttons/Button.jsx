import "./Button.scss";
import PropTypes from "prop-types";

function AddToCart({
  backgroundColor = "#742232",
  text,
  openModalfunc,
  addToCartFunc,
}) {
  const bgColor = {
    backgroundColor: backgroundColor,
  };

  return (
    <button
      className="button"
      style={bgColor}
      onClick={() => {
        addToCartFunc();
        openModalfunc();
      }}
    >
      {text}
    </button>
  );
}

AddToCart.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string.isRequired,
  openModalfunc: PropTypes.func.isRequired,
  addToCartFunc: PropTypes.func.isRequired,
};

export default AddToCart;
