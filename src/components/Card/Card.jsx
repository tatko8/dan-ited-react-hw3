import "./Card.scss";
import PropTypes from "prop-types";
import IconFav from "../IconFav/IconFav";
import Buttons from "../Buttons/Button";
const Card = ({
  id,
  image,
  name,
  price,
  vin,
  toOpenModal,
  tryToAddToCart,
  addProdToFavourite,
  removeProdFromFavourite,
  favorite,
  btnMessage,
}) => {
  const thisCard = { id, name, price, image, vin };

  const tryToAddProdToCart = () => {
    tryToAddToCart(thisCard);
  };
  const addToFav = () => {
    addProdToFavourite(thisCard);
  };
  const removeFromFav = () => {
    removeProdFromFavourite(thisCard.id);
  };
  const favoritesChecker = (id) => {
    const boolean = favorite.some((product) => product.id === id);
    return boolean;
  };

  return (
    <>
      <div className="card">
        <li className="card__item">
          <div className="card__image">
            <img
              className="card__image"
              src={image}
              alt="card of product"
            ></img>
          </div>
          <p className="card__name">{name} </p>
          <p className="card__vin"> Article: {vin}</p>
          <p className="card__price">
            {new Intl.NumberFormat("en-US", {
              style: "currency",
              currency: "UAH",
              currencyDisplay: "narrowSymbol",
            }).format(price)}
          </p>
          <div className="card__action-btns">
            <Buttons
              backgroundColor="#3f3a03"
              text={btnMessage}
              openModalfunc={toOpenModal}
              addToCartFunc={tryToAddProdToCart}
            />
            <IconFav
              addProdToFavourite={addToFav}
              removeProdFromFavourite={removeFromFav}
              thisCard={thisCard}
              favoritesChecker={favoritesChecker}
            />
          </div>
        </li>
      </div>
    </>
  );
};

Card.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  vin: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired,
  toOpenModal: PropTypes.func.isRequired,
  tryToAddToCart: PropTypes.func.isRequired,
  addProdToFavourite: PropTypes.func.isRequired,
  removeProdFromFavourite: PropTypes.func.isRequired,
  favorite: PropTypes.array.isRequired,
  btnMessage: PropTypes.string.isRequired,
};
export default Card;
